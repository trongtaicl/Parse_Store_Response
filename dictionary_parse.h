/*******************************************************************************
 *  (C) Copyright 2009 Molisys Solutions Co., Ltd. , All rights reserved       *
 *                                                                             *
 *  This source code and any compilation or derivative thereof is the sole     *
 *  property of Molisys Solutions Co., Ltd. and is provided pursuant to a      *
 *  Software License Agreement.  This code is the proprietary information      *
 *  of Molisys Solutions Co., Ltd and is confidential in nature.  Its use and  *
 *  dissemination by any party other than Molisys Solutions Co., Ltd is        *
 *  strictly limited by the confidential information provisions of the         *
 *  Agreement referenced above.                                                *
 ******************************************************************************/

/*
 * dictionary_parse.h
 *
 *  Created on: 24 thg 8, 2017
 *      Author: Tai
 *  @brief define tag name in dictionary and function that be used
 */


#ifndef DICTIONARY_PARSE_H_
#define DICTIONARY_PARSE_H_


#ifdef __cplusplus
extern "C"
{
#endif
/********** Include section ***************************************************/
#include <stdlib.h>

/********** Type definition section *******************************************/
/**
 * @brief Enumeration of tag name in dictionary
 */

#define MAX_ROW 300
#define MAX_COL 300
#define LengExcess 100

typedef enum {
	Dic_ERROR                     = -1,
	Dic_Acc_Type                  = 21,
	Dic_Acq_Id                    = 30,
	Dic_Add_Ter_Cap               = 31,
	Dic_Amo_Aut                   = 32,
	Dic_Amo_Oth                   = 33,
	Dic_App_Cap_Inf               = 34,
	Dic_App_Cry                   = 35,
	Dic_App_Cur_Code              = 36,
	Dic_App_Cur_Exp               = 37,
	Dic_App_Eff_Date              = 22,
	Dic_Card_Data_Inp_Cap         = 117,
	Dic_App_Exp_Date              = 23,
	Dic_App_File_Loc              = 0,
	Dic_App_Int_Pro               = 1,
	Dic_App_Lab                   = 2,
	Dic_App_Pre_Name              = 38,
	Dic_App_PAN                   = 3,
	Dic_App_PAN_Seq               = 24,
	Dic_App_Pri_Ind               = 4,
	Dic_App_Trans_Cou             = 39,
	Dic_App_Usa_Con               = 40,
	Dic_App_Vers_Num_C            = 41,
	Dic_App_Vers_Num_R            = 42,
	Dic_Bal_Read_Bef_Gen_AC       = 115,
	Dic_Bal_Read_Aft_Gen_AC       = 116,
	Dic_CA_Pub_Key_Ind_C          = 5,
	Dic_CDOL1                     = 6,
	Dic_CDOL1_Rel_Data            = 118,
	Dic_Cry_Inf_Data              = 43,
	Dic_CVC3_Track1               = 44,
	Dic_CVC3_Track2               = 45,
	Dic_CVM_Cap_CVM_Req           = 119,
	Dic_CVM_Cap_CVM_No_Req        = 120,
	Dic_CVM_List                  = 7,
	Dic_CVM_Res                   = 46,
	Dic_Data_Nee                  = 121,
	Dic_Data_Rec                  = 174,
	Dic_Data_To_Send              = 175,
	Dic_DD_Card_Track1            = 122,
	Dic_DD_Card_Track2            = 123,
	Dic_Def_UDOL                  = 124,
	Dic_Dev_Est_Trans_Time        = 167,
	Dic_Dev_Rel_Res_Ent           = 168,
	Dic_DF_Name                   = 8,
	Dic_Dis_Data                  = 176,
	Dic_DRDOL                     = 47,
	Dic_DRDOL_Rel_Data            = 125,
	Dic_DS_AC_Type                = 126,
	Dic_DS_Dig_H                  = 110,
	Dic_DSDOL                     = 48,
	Dic_DS_ID                     = 49,
	Dic_DS_Inp_Card               = 111,
	Dic_DS_Inp_Term               = 127,
	Dic_DS_ODS_Card               = 50,
	Dic_DS_ODS_Inf                = 112,
	Dic_DS_ODS_Inf_For_R          = 128,
	Dic_DS_ODS_Term               = 113,
	Dic_DS_Req_Ope                = 51,
	Dic_DS_Slot_Ava               = 52,
	Dic_DS_Slot_Man               = 53,
	Dic_DS_Sum_1                  = 54,
	Dic_DS_Sum_2                  = 129,
	Dic_DS_Sum_3                  = 130,
	Dic_DS_Sum_Sta                = 131,
	Dic_DS_Unp_Num                = 55,
	Dic_DSVN_Term                 = 132,
	Dic_Err_Ind                   = 133,
	Dic_File_Con_Inf_Iss          = 109,
	Dic_File_Con_Inf_Pro          = 9,
	Dic_File_Con_Inf_Tem          = 10,
	Dic_Hold_Time_Val             = 134,
	Dic_ICC_Dyn_Num               = 56,
	Dic_ICC_Pub_Key_Cer           = 57,
	Dic_ICC_Pub_Key_Exp           = 58,
	Dic_ICC_Pub_Key_Rem           = 59,
	Dic_IDS_Sta                   = 135,
	Dic_Int_Dev_Ser_Num           = 60,
	Dic_Iss_Act_Code_Def          = 61,
	Dic_Iss_Act_Code_Den          = 62,
	Dic_Iss_Act_Code_Onl          = 63,
	Dic_Iss_App_Data              = 64,
	Dic_Iss_Code_Tab_Ind          = 65,
	Dic_Iss_Cou_Code              = 25,
	Dic_Iss_Pub_Key_Cer           = 11,
	Dic_Iss_Pub_Key_Exp           = 66,
	Dic_Iss_Pub_Key_Rem           = 12,
	Dic_Ker_Con                   = 136,
	Dic_Ker_ID                    = 137,
	Dic_Lan_Pre                   = 26,
	Dic_Log_Ent                   = 67,
	Dic_Mag_str_App_Vers_Num      = 68,
	Dic_Mag_Str_CVM_Cap_CVM_Req   = 138,
	Dic_Mag_str_CVM_Cap_No_CVM_Req = 139,
	Dic_Max_Rel_Res_Grac_Per       = 140,
	Dic_Max_Time_For_Pro_Rel_Res_APDU = 169,
	Dic_Max_Lif_Of_Tor_Trans_Log_Rec = 141,
	Dic_Max_Num_Of_Tor_Trans_Log_Rec = 142,
	Dic_Mea_Rel_Res_Pro_Time         = 170,
	Dic_Mer_Cat_Code              = 69,
	Dic_Mer_Cus_Data              = 70,
	Dic_Mer_Id                    = 71,
	Dic_Mer_Name_And_Loc          = 72,
	Dic_Mes_Hold_Time             = 143,
	Dic_Min_Rel_Res_Gra_Per       = 144,
	Dic_Min_Time_For_Pro_Rel_Res_APDU  = 171,
	Dic_Mob_Sup_Ind               = 73,
	Dic_NATC_Track1               = 74,
	Dic_NATC_Track2               = 75,
	Dic_Off_Acc_Bal               = 76,
	Dic_Out_Par_Set               = 145,
	Dic_Pay_Acc_Ref               = 77,
	Dic_PCVC3_Track1              = 78,
	Dic_PCVC3_Track2              = 79,
	Dic_PDOL                      = 80,
	Dic_PDOL_Rel_Data             = 146,
	Dic_Phone_Mes_Tab             = 147,
	Dic_POS_Card_Int_Inf          = 114,
	Dic_Pos_Gen_AC_Put_Data_Sta   = 148,
	Dic_Pre_Gen_Ac_Put_Data_Sta   = 149,
	Dic_Pro_To_Fir_Wri_Flag       = 150,
	Dic_Pro_Data_Env_1            = 81,
	Dic_Pro_Data_Env_2            = 82,
	Dic_Pro_Data_Env_3            = 83,
	Dic_Pro_Data_Env_4            = 84,
	Dic_Pro_Data_Env_5            = 85,
	Dic_PUNATC_Track1             = 86,
	Dic_PUNATC_Track2             = 87,
	Dic_Read_Con_Flo_Lim          = 151,
	Dic_Read_Con_Trans_Lim_No_CVM = 152,
	Dic_Read_Con_Trans_Lim_CVM    = 153,
	Dic_Read_CVM_Req_Lim          = 154,
	Dic_Read_Rec_Res_Mes_Tem      = 13,
	Dic_Ref_Con_Par               = 155,
	Dic_Rel_Res_Acc_Thr           = 156,
	Dic_Rel_Res_Trans_Time_Mis_Thr= 157,
	Dic_Res_Mes_Tem_For1          = 14,
	Dic_Res_Mes_Tem_For2          = 15,
	Dic_RRP_Counter               = 172,
	Dic_Ser_Cap                   = 158,
	Dic_Ser_Code                  = 27,
	Dic_Sig_Dyn_App_Data          = 88,
	Dic_Sta_Data_Aut_Tag_List     = 89,
	Dic_Tags_To_Read              = 159,
	Dic_Tags_To_Wri_Aft_Gen_AC    = 177,
	Dic_Tags_To_Wri_Bef_Gen_AC    = 178,
	Dic_Ter_Act_Code_Def          = 160,
	Dic_Ter_Act_Code_Den          = 161,
	Dic_Ter_Act_Code_Onl          = 162,
	Dic_Ter_Cap                   = 90,
	Dic_Ter_Cou_Code              = 91,
	Dic_CAPDU                     = 163,
	Dic_RAPDU                     = 164,
	Dic_Ter_Id                    = 92,
	Dic_Ter_Rel_Res_Ent           = 173,
	Dic_Ter_Risk_Man_Data         = 93,
	Dic_Ter_Type                  = 94,
	Dic_Ter_Ver_Res               = 16,
	Dic_Thi_Par_Data                      = 95,
	Dic_Time_Out_Val                      = 165,
	Dic_Torn_Rec                          = 179,
	Dic_Track1_Data                       = 17,
	Dic_Track1_Dis_Data                   = 96,
	Dic_Track2_Data                       = 97,
	Dic_Track2_Dis_Data                   = 98,
	Dic_Track2_Equi_Data                  = 18,
	Dic_Trans_Cat_Code                    = 99,
	Dic_Trans_Cur_Code                    = 28,
	Dic_Trans_Cur_Exp                     = 29,
	Dic_Trans_Date                        = 19,
	Dic_Trans_Time                        = 100,
	Dic_Trans_Type                        = 20,
	Dic_UDOL                              = 101,
	Dic_Unp_Num                           = 102,
	Dic_Unp_Num_Num                       = 103,
	Dic_Unp_Data_Env_1                    = 104,
	Dic_Unp_Data_Env_2                     = 105,
	Dic_Unp_Data_Env_3                    = 106,
	Dic_Unp_Data_Env_4                    = 107,
	Dic_Unp_Data_Env_5                    = 108,
	Dic_Use_Int_Req_Data                  = 166
} Dic_Tag;
/**
 * @brief Enumeration of result after checking some rules of tag in dicitonary
 */

typedef enum { DicCheck_TRUE =1,
	           DicCheck_FALSE =0,
	           DicCheck_NotKnown = 2
}DicCheck_Boolean;
typedef enum { DatabaseCheck_TRUE =1,
	           DatabaseCheck_FALSE =0
}DatabaseCheck_Boolean;

typedef enum{
CheckConstruct,
CheckPrimiti
}CheckType;

/**
 * @brief exact-width unsigned integer types
 */
typedef unsigned          char uint8_t;
typedef unsigned short     int uint16_t;
typedef unsigned           int uint32_t;


/********** Function declaration section **************************************/
/**
 * @brief Check if the tag is exist in dictionary
 * @param pointer to Tag T, Tag T is the ordinal number of tag that need to check in the buffer
 * @retval 0: false
 *         1: Tag is exist
 */

DicCheck_Boolean IsKnown(int T);

/**
 * @brief Check if the length of tag T is between LengMin and LengMax
 * @param Tag T is the ordinal number of tag that need to check in the buffer
 * @retval 0: false
 *         1: Length of tag T is suitable
 */
DicCheck_Boolean CheckLength( Dic_Tag T, uint8_t Length);

/**
 * @brief check if the tag that need to check has right template
 * @param Tag T is the ordinal number of tag that need to check in the buffer
 * @retval 0: false
 *         1: tag has right template
 */

DicCheck_Boolean CheckTemplate( Dic_Tag T, int Tem);
/**
* @brief check that tag is in the RA signal
* @param byte1, byte2, byte3 are three first bytes of tag
* @retval 0:false
*         1: tag is in the RA signal
*/
DicCheck_Boolean CheckRASignal (Dic_Tag T);


/**
* @brief Check tag is exist in dictionary and RA signal, take value of tag T (is defined in enumeration) to check later
* @param byte1, byte2, byte3 are three first bytes of tag
* @retval LengthMin, LengthMax of tag
*
*/

int CheckIsKnownRASignal(uint8_t byte1, uint8_t byte2, uint8_t byte3, int *stt);

/**
* @brief check tag T is empty in database or not
*
*/
DatabaseCheck_Boolean IsEmpty(Dic_Tag T, uint8_t Database_Array[][MAX_COL]);
/**
*
*/

DatabaseCheck_Boolean IsNotPresent(Dic_Tag T, uint8_t Database_Array[][MAX_COL], uint8_t Sobyte, uint8_t *Buffertam, uint8_t Vitritag);
/**
*
*/

DatabaseCheck_Boolean IsNotPresent(Dic_Tag T, uint8_t Database_Array[][MAX_COL], uint8_t Sobyte, uint8_t *Buffertam, uint8_t Vitritag);
/**
*
*/

DatabaseCheck_Boolean IsPresent(Dic_Tag T, uint8_t Database_Array[][MAX_COL], uint8_t Sobyte, uint8_t *Buffertam, uint8_t Vitritag);
/**
*
*/

int Check(uint8_t *Buffertam, uint16_t *Length, uint8_t *Sobyte, CheckType *CheckTypeTag, int Vitri);

#ifdef __cplusplus
}
#endif
#endif /* DICTIONARY_PARSE_H_ */
