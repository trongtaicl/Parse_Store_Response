/*******************************************************************************
 *  (C) Copyright 2009 Molisys Solutions Co., Ltd. , All rights reserved       *
 *                                                                             *
 *  This source code and any compilation or derivative thereof is the sole     *
 *  property of Molisys Solutions Co., Ltd. and is provided pursuant to a      *
 *  Software License Agreement.  This code is the proprietary information      *
 *  of Molisys Solutions Co., Ltd and is confidential in nature.  Its use and  *
 *  dissemination by any party other than Molisys Solutions Co., Ltd is        *
 *  strictly limited by the confidential information provisions of the         *
 *  Agreement referenced above.                                                *
 ******************************************************************************/
/*
 * dictionary_parse.c
 *
 *  Created on: 24, 8, 2017
 *      Author: Tai
 *  @brief Check if the length of tag in TLV string is compliant with length condition in dictionary
 *         Check if the tag has is exist in dictionary
 */

/********** Include section ***************************************************/
#include "dictionary_parse.h"
#include <stdlib.h>
#include <stdio.h>
/********** Local (static) variable definition section ************************/

static int LengMin[MAX_COL], LengMax[MAX_COL];

static DicCheck_Boolean RA[MAX_COL];

static uint8_t Template[MAX_COL], Template2[MAX_COL];
/********** Local (static) function declaration section ***********************/

/********** Global function definition section *********************************/

//************* check if tag T is exist in dictionary **************

DicCheck_Boolean IsKnown(int T){
	if (T!= Dic_ERROR) return DicCheck_TRUE;
        printf("fail isknown %d\n", T);
        return DicCheck_FALSE;
}

DicCheck_Boolean CheckRASignal (Dic_Tag T){
        if (RA[T] == DicCheck_TRUE){
            return DicCheck_TRUE;
        }
        else
            printf("cai nay sai2");
            return DicCheck_FALSE;
}

//******* check template *********
DicCheck_Boolean CheckTemplate(Dic_Tag T, int Tem){
	if ((Template[T]== 0x00) && (Template2[T]== 0x00)) { return DicCheck_FALSE;}
	else {
	if((Tem==Template[T])||(Tem ==Template2[T])) return DicCheck_TRUE;
	else
        printf("fail template %d \n", T);
        return DicCheck_FALSE;
}
}

DicCheck_Boolean CheckLength( Dic_Tag T, uint8_t Length) {
	if (T == Dic_App_File_Loc){
		if (((Length) % 4 == 0) && ((Length >= LengMin[T]) && (Length <= LengMax[T]))) {
	       return DicCheck_TRUE;
		}
	}
	else if ((T == Dic_CDOL1_Rel_Data) || (T == Dic_Data_Nee)|| (T == Dic_Data_Rec)|| (T == Dic_Data_To_Send)|| (T == Dic_Dis_Data)|| (T == Dic_DRDOL_Rel_Data)||
			(T == Dic_DSVN_Term)||(T == Dic_Mer_Name_And_Loc)|| (T == Dic_PDOL_Rel_Data)||(T == Dic_Phone_Mes_Tab)||(T == Dic_Tags_To_Read)||
			(T == Dic_Tags_To_Wri_Aft_Gen_AC)|| (T == Dic_Tags_To_Wri_Bef_Gen_AC)||(T == Dic_Torn_Rec)) {
		return DicCheck_TRUE;
	}
	else if ((T == Dic_DS_Sum_1)||(T == Dic_DS_Sum_2) ||(T == Dic_DS_Sum_3)){
		if ((Length==8)|| (Length== 16)){
			return DicCheck_TRUE;
		}
	}
	else if ((T == Dic_ICC_Pub_Key_Exp)||(T == Dic_Iss_Pub_Key_Exp)){
		if ((Length == 1)|| (Length==3)){
			return DicCheck_TRUE;
		}
	}
	else {
		if ((Length >= LengMin[T])&& (Length <= LengMax[T])){
           return DicCheck_TRUE;
		}
	}
	printf(" cai nay sai1");
	return DicCheck_FALSE;
}

// ****check to save the position of tag T and from that position, tag T will be saved in two dimensional array Database
int CheckIsKnownRASignal(uint8_t byte1, uint8_t byte2, uint8_t byte3, int *stt){
    switch(byte1){
          case 0x94: *stt= Dic_App_File_Loc ; LengMin[*stt]=4 ;LengMax[*stt]=248; Template[*stt]= 0x77; RA[*stt]= DicCheck_TRUE;
        	  break;
          case 0x82: *stt= Dic_App_Int_Pro; LengMin[*stt]=2 ;LengMax[*stt]=2; Template[*stt]= 0x77; RA[*stt]= DicCheck_TRUE;
              break;
          case 0x50: *stt=Dic_App_Lab; LengMin[*stt]=0 ;LengMax[*stt]=16; Template[*stt]= 0xA5; RA[*stt]=DicCheck_TRUE;
              break;
          case 0x5A: *stt= Dic_App_PAN; LengMin[*stt]=0 ;LengMax[*stt]=10; Template[*stt]=0x77;Template2[*stt]=0x70;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x87: *stt=Dic_App_Pri_Ind; LengMin[*stt]=1 ;LengMax[*stt]=1; Template[*stt]=0xA5;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x8F: *stt=Dic_CA_Pub_Key_Ind_C; LengMin[*stt]=1 ;LengMax[*stt]=1; Template[*stt]=0x77;Template2[*stt]=0x70;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x8C: *stt=Dic_CDOL1; LengMin[*stt]=0 ;LengMax[*stt]=250; Template[*stt]=0x77;Template2[*stt]=0x70;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x8E: *stt=Dic_CVM_List ; LengMin[*stt]=10 ;LengMax[*stt]=250; Template[*stt]=0x77;Template2[*stt]=0x70;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x84: *stt=Dic_DF_Name; LengMin[*stt]=5 ;LengMax[*stt]=16; Template[*stt]= 0x6F; RA[*stt]=DicCheck_TRUE;
              break;
          case 0xA5: *stt=Dic_File_Con_Inf_Pro; LengMin[*stt]=0 ;LengMax[*stt]=240; Template[*stt]= 0x6F ;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x6F: *stt= Dic_File_Con_Inf_Tem; LengMin[*stt]=0 ;LengMax[*stt]=250; RA[*stt]=DicCheck_TRUE;
              break;
          case 0x90: *stt=Dic_Iss_Pub_Key_Cer; LengMin[*stt]=0 ;LengMax[*stt]=248; Template[*stt]=0x77;Template2[*stt]=0x70;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x92: *stt=Dic_Iss_Pub_Key_Rem; LengMin[*stt]=0 ;LengMax[*stt]=256; Template[*stt]=0x77;Template2[*stt]=0x70;RA[*stt]=DicCheck_TRUE; // Length=Ni-Nca+36
              break;
          case 0x70: *stt=Dic_Read_Rec_Res_Mes_Tem;LengMin[*stt]=0 ;LengMax[*stt]=253;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x80: *stt=Dic_Res_Mes_Tem_For1;LengMin[*stt]=0 ;LengMax[*stt]=253;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x77: *stt=Dic_Res_Mes_Tem_For2;LengMin[*stt]=0 ;LengMax[*stt]=253;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x95: *stt=Dic_Ter_Ver_Res;LengMin[*stt]=5 ;LengMax[*stt]=5;
              break;
          case 0x56: *stt=Dic_Track1_Data;LengMin[*stt]=0 ;LengMax[*stt]=76; Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
              break;
          case 0x57: *stt=Dic_Track2_Equi_Data;LengMin[*stt]=0 ;LengMax[*stt]=19; Template2[*stt]=0x70;Template[*stt]=0x77;RA[*stt]=DicCheck_TRUE;
              break;
          case 0x9A: *stt=Dic_Trans_Cur_Exp;LengMin[*stt]=3 ;LengMax[*stt]=3;
              break;
          case 0x9C: *stt=Dic_Trans_Type;LengMin[*stt]=1 ;LengMax[*stt]=1;
              break;
          case 0x5F:  switch(byte2){
                           case 0x57: *stt= Dic_Acc_Type ;LengMin[*stt]=1 ;LengMax[*stt]=1;
                               break;
                           case 0x25: *stt=Dic_App_Eff_Date ;LengMin[*stt]=3 ;LengMax[*stt]=3;Template2[*stt]=0x70;Template[*stt]=0x77;RA[*stt]=DicCheck_TRUE;
                               break;
                           case 0x24: *stt=Dic_App_Exp_Date;LengMin[*stt]=3 ;LengMax[*stt]=3;Template2[*stt]=0x70;Template[*stt]=0x77;RA[*stt]=DicCheck_TRUE;
                               break;
                           case 0x34: *stt=Dic_App_PAN_Seq;LengMin[*stt]=1 ;LengMax[*stt]=1;Template2[*stt]=0x70;Template[*stt]=0x77;RA[*stt]=DicCheck_TRUE;
                               break;
                           case 0x28: *stt=Dic_Iss_Cou_Code;LengMin[*stt]=2 ;LengMax[*stt]=2;Template2[*stt]=0x70;Template[*stt]=0x77;RA[*stt]=DicCheck_TRUE;
                               break;
                           case 0x2D: *stt=Dic_Lan_Pre ;LengMin[*stt]=2 ;LengMax[*stt]=8;Template[*stt]=0xA5;RA[*stt]=1;
                               break;
                           case 0x30: *stt=Dic_Ser_Code ;LengMin[*stt]=2 ;LengMax[*stt]=2;Template2[*stt]=0x70;Template[*stt]=0x77;RA[*stt]=DicCheck_TRUE;
                               break;
                           case 0x2A: *stt=Dic_Trans_Cur_Code  ;LengMin[*stt]=2 ;LengMax[*stt]=2;
                               break;
                           case 0x36: *stt=Dic_Trans_Cur_Exp;LengMin[*stt]=1 ;LengMax[*stt]=1;
                               break;
                           default: *stt = Dic_ERROR;
                               break;
                     }
              break;
          case 0x9F: switch(byte2){
                           case 0x01: *stt=Dic_Acq_Id ;LengMin[*stt]=6 ;LengMax[*stt]=6;
                                break;
                           case 0x40: *stt=Dic_Add_Ter_Cap;LengMin[*stt]=5 ;LengMax[*stt]=5;
                                break;
                           case 0x02: *stt=Dic_Amo_Aut;LengMin[*stt]=6 ;LengMax[*stt]=6;
                                break;
                           case 0x03: *stt=Dic_Amo_Oth ;LengMin[*stt]=6 ;LengMax[*stt]=6;
                                break;
                           case 0x5D: *stt=Dic_App_Cap_Inf;LengMin[*stt]=3 ; LengMax[*stt]=3; Template[*stt] = 0xBF; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x26: *stt=Dic_App_Cry;LengMin[*stt]=8 ;LengMax[*stt]=8;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x42: *stt=Dic_App_Cur_Code ;LengMin[*stt]=2 ;LengMax[*stt]=2;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x44: *stt=Dic_App_Cur_Exp ;LengMin[*stt]=1 ;LengMax[*stt]=1;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x12: *stt=Dic_App_Pre_Name;LengMin[*stt]=0 ;LengMax[*stt]=16;Template[*stt]=0xA5; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x36: *stt=Dic_App_Trans_Cou;LengMin[*stt]=2 ;LengMax[*stt]=2;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x07: *stt=Dic_App_Usa_Con;LengMin[*stt]=2 ;LengMax[*stt]=2;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x08: *stt=Dic_App_Vers_Num_C;LengMin[*stt]=2 ;LengMax[*stt]=2;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x09: *stt=Dic_App_Vers_Num_R;LengMin[*stt]=2 ;LengMax[*stt]=2;
                                break;
                           case 0x27: *stt=Dic_Cry_Inf_Data;LengMin[*stt]=1 ;LengMax[*stt]=1;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x60: *stt=Dic_CVC3_Track1;LengMin[*stt]=2 ;LengMax[*stt]=2;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x61: *stt=Dic_CVC3_Track2 ;LengMin[*stt]=2 ;LengMax[*stt]=2;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x34: *stt=Dic_CVM_Res ;LengMin[*stt]=3 ;LengMax[*stt]=3;
                                break;
                           case 0x51: *stt=Dic_DRDOL ;LengMin[*stt]=0 ;LengMax[*stt]=250;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]= DicCheck_TRUE;
                                break;
                           case 0x5B: *stt=Dic_DSDOL;LengMin[*stt]=0 ;LengMax[*stt]=250;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x5E: *stt=Dic_DS_ID ;LengMin[*stt]=8 ;LengMax[*stt]=11;Template[*stt]=0xBF; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x54: *stt=Dic_DS_ODS_Card;LengMin[*stt]=0 ;LengMax[*stt]=160;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x5C: *stt=Dic_DS_Req_Ope ;LengMin[*stt]=8 ;LengMax[*stt]=8;
                                break;
                           case 0x5F: *stt=Dic_DS_Slot_Ava;LengMin[*stt]=1 ;LengMax[*stt]=1;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x6F: *stt=Dic_DS_Slot_Man;LengMin[*stt]=1 ;LengMax[*stt]=1;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x7D: *stt=Dic_DS_Sum_1 ;LengMin[*stt]=8 ;LengMax[*stt]=16;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x7F: *stt=Dic_DS_Unp_Num ;LengMin[*stt]=4 ;LengMax[*stt]=4;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x4C: *stt=Dic_ICC_Dyn_Num;LengMin[*stt]=2 ;LengMax[*stt]=8; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x46: *stt=Dic_ICC_Pub_Key_Cer;LengMin[*stt]=0 ;LengMax[*stt]=248;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x47: *stt=Dic_ICC_Pub_Key_Exp;LengMin[*stt]=1 ;LengMax[*stt]=3;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;// tru leng=2
                                break;
                           case 0x48: *stt=Dic_ICC_Pub_Key_Rem;LengMin[*stt]=0 ;LengMax[*stt]=255;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x1E: *stt=Dic_Int_Dev_Ser_Num;LengMin[*stt]=8 ;LengMax[*stt]=8;
                                break;
                           case 0x0D: *stt=Dic_Iss_Act_Code_Def;LengMin[*stt]=5 ;LengMax[*stt]=5;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x0E: *stt=Dic_Iss_Act_Code_Den;LengMin[*stt]=5 ;LengMax[*stt]=5;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x0F: *stt=Dic_Iss_Act_Code_Onl;LengMin[*stt]=5 ;LengMax[*stt]=5;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x10: *stt=Dic_Iss_App_Data;LengMin[*stt]=0 ;LengMax[*stt]=32;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x11: *stt=Dic_Iss_Code_Tab_Ind ;LengMin[*stt]=1 ;LengMax[*stt]=1;Template[*stt]=0xA5; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x32: *stt=Dic_Iss_Pub_Key_Exp;LengMin[*stt]=1 ;LengMax[*stt]=3;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE; //1 hoac 3
                                break;
                           case 0x4D: *stt=Dic_Log_Ent;LengMin[*stt]=2 ;LengMax[*stt]=2;Template[*stt]=0xBF; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x6D: *stt=Dic_Mag_str_App_Vers_Num;LengMin[*stt]=2 ;LengMax[*stt]=2;
                                break;
                           case 0x15: *stt=Dic_Mer_Cat_Code;LengMin[*stt]=2 ;LengMax[*stt]=2;
                                break;
                           case 0x7C: *stt=Dic_Mer_Cus_Data;LengMin[*stt]=20 ;LengMax[*stt]=20;
                                break;
                           case 0x16: *stt=Dic_Mer_Id;LengMin[*stt]=15 ;LengMax[*stt]=15;
                                break;
                           case 0x4E: *stt=Dic_Mer_Name_And_Loc;LengMin[*stt]=0 ;LengMax[*stt]=255;
                                break;
                           case 0x7E: *stt=Dic_Mob_Sup_Ind;LengMin[*stt]=1 ;LengMax[*stt]=1;
                                break;
                           case 0x64: *stt=Dic_NATC_Track1;LengMin[*stt]=1 ;LengMax[*stt]=1;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x67: *stt=Dic_NATC_Track2;LengMin[*stt]=1 ;LengMax[*stt]=1;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x50: *stt=Dic_Off_Acc_Bal;LengMin[*stt]=6 ;LengMax[*stt]=6; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x24: *stt=Dic_Pay_Acc_Ref;LengMin[*stt]=29 ;LengMax[*stt]=29;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x62: *stt=Dic_PCVC3_Track1;LengMin[*stt]=6 ;LengMax[*stt]=6;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x65: *stt=Dic_PCVC3_Track2;LengMin[*stt]=2 ;LengMax[*stt]=2;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x38: *stt=Dic_PDOL;LengMin[*stt]=0 ;LengMax[*stt]=240;Template[*stt]=0xA5; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x70: *stt=Dic_Pro_Data_Env_1;LengMin[*stt]=0 ;LengMax[*stt]=192; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x71: *stt=Dic_Pro_Data_Env_2;LengMin[*stt]=0 ;LengMax[*stt]=192; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x72: *stt=Dic_Pro_Data_Env_3;LengMin[*stt]=0 ;LengMax[*stt]=192; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x73: *stt=Dic_Pro_Data_Env_4 ;LengMin[*stt]=0 ;LengMax[*stt]=192; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x74: *stt=Dic_Pro_Data_Env_5;LengMin[*stt]=0 ;LengMax[*stt]=192; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x63: *stt=Dic_PUNATC_Track1 ;LengMin[*stt]=6 ;LengMax[*stt]=6;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x66: *stt=Dic_PUNATC_Track2;LengMin[*stt]=2 ;LengMax[*stt]=2;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x4B: *stt=Dic_Sig_Dyn_App_Data;LengMin[*stt]=2 ;LengMax[*stt]=2;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x4A: *stt=Dic_Sta_Data_Aut_Tag_List;LengMin[*stt]=0 ;LengMax[*stt]=250;Template2[*stt]=0x70;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x33: *stt=Dic_Ter_Cap;LengMin[*stt]=3 ;LengMax[*stt]=3;
                                break;
                           case 0x1A: *stt=Dic_Ter_Cou_Code;LengMin[*stt]=2 ;LengMax[*stt]=2;
                                break;
                           case 0x1C: *stt=Dic_Ter_Id;LengMin[*stt]=8 ;LengMax[*stt]=8;
                                break;
                           case 0x1D: *stt=Dic_Ter_Risk_Man_Data;LengMin[*stt]=8 ;LengMax[*stt]=8;
                                break;
                           case 0x35: *stt=Dic_Ter_Type;LengMin[*stt]=1 ;LengMax[*stt]=1;
                                break;
                           case 0x6E: *stt=Dic_Thi_Par_Data;LengMin[*stt]=5 ; LengMax[*stt]=32; Template[*stt]=0xBF; Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x1F: *stt=Dic_Track1_Dis_Data;LengMin[*stt]=0 ;LengMax[*stt]=54;Template[*stt]=0x77;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x6B: *stt=Dic_Track2_Data;LengMin[*stt]=0 ;LengMax[*stt]=19;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x20: *stt=Dic_Track2_Dis_Data;LengMin[*stt]=0 ;LengMax[*stt]=16;Template2[*stt]=0x70;Template[*stt]=0x77; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x53: *stt=Dic_Trans_Cat_Code;LengMin[*stt]=1 ;LengMax[*stt]=1;
                                break;
                           case 0x21: *stt=Dic_Trans_Time;LengMin[*stt]=3 ;LengMax[*stt]=3;
                                break;
                           case 0x69: *stt=Dic_UDOL;LengMin[*stt]=0 ;LengMax[*stt]=250;Template2[*stt]=0x70; RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x37: *stt=Dic_Unp_Num ;LengMin[*stt]=4 ;LengMax[*stt]=4;
                                break;
                           case 0x6A: *stt=Dic_Unp_Num_Num;LengMin[*stt]=4 ;LengMax[*stt]=4;
                                break;
                           case 0x75: *stt=Dic_Unp_Data_Env_1;LengMin[*stt]=0 ;LengMax[*stt]=192;RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x76: *stt=Dic_Unp_Data_Env_2;LengMin[*stt]=0 ;LengMax[*stt]=192;RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x77: *stt=Dic_Unp_Data_Env_3;LengMin[*stt]=0 ;LengMax[*stt]=192;RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x78: *stt=Dic_Unp_Data_Env_4;LengMin[*stt]=0 ;LengMax[*stt]=192;RA[*stt]=DicCheck_TRUE;
                                break;
                           case 0x79: *stt=Dic_Unp_Data_Env_5;LengMin[*stt]=0 ;LengMax[*stt]=192;RA[*stt]=DicCheck_TRUE;
                                break;
                           default: *stt = Dic_ERROR;
                                break;
                     }
              break;
          case 0xBF: switch(byte2){
                           case 0x0C: *stt=Dic_File_Con_Inf_Iss ; LengMin[*stt]=0 ;LengMax[*stt]=192;Template[*stt]=0xA5;RA[*stt]=DicCheck_TRUE;
                               break;
                           default: *stt = Dic_ERROR;
                               break;
                     }
              break;
          case 0xDF: switch(byte2){
                           case 0x61: *stt=Dic_DS_Dig_H;LengMin[*stt]=8 ;LengMax[*stt]=8;
                        	   break;
                           case 0x60: *stt=Dic_DS_Inp_Card;LengMin[*stt]=8 ;LengMax[*stt]=8;//DS Input (Card)
                        	   break;
                           case 0x62: *stt=Dic_DS_ODS_Inf;LengMin[*stt]=1 ;LengMax[*stt]=1;//DS ODS Info
                        	   break;
                           case 0x63: *stt=Dic_DS_ODS_Term ;LengMin[*stt]=0 ;LengMax[*stt]=160;//DS ODS Term
                        	   break;
                           case 0x4B: *stt=Dic_POS_Card_Int_Inf;LengMin[*stt]=3 ;LengMax[*stt]=3;Template[*stt]=0x77;RA[*stt]=1;// Interaction Information
                        	   break;
                           case 0x81:  switch(byte3){
                                            case 0x04 : *stt=Dic_Bal_Read_Bef_Gen_AC;LengMin[*stt]=6 ;LengMax[*stt]=6;//Balance Read Before Gen AC
                                    	        break;
                                            case 0x05 : *stt=Dic_Bal_Read_Aft_Gen_AC;LengMin[*stt]=6 ;LengMax[*stt]=6;//Balance Read After Gen AC
                                    	        break;
                                            case 0x17 : *stt=Dic_Card_Data_Inp_Cap;LengMin[*stt]=1 ;LengMax[*stt]=1;//Card Data Input Capability
                                    	        break;
                                            case 0x07 : *stt=Dic_CDOL1_Rel_Data;LengMin[*stt]=0 ;LengMax[*stt]=255;//CDOL1 Related Data
                                    	        break;
                                            case 0x18 : *stt=Dic_CVM_Cap_CVM_Req;LengMin[*stt]=1 ;LengMax[*stt]=1;//CVM Capability CVM Required
                                    	        break;
                                            case 0x19 : *stt=Dic_CVM_Cap_CVM_No_Req;LengMin[*stt]=1 ;LengMax[*stt]=1;//CVM Capability � No CVM Required
                                    	        break;
                                            case 0x06 : *stt=Dic_Data_Nee;LengMin[*stt]=0 ;LengMax[*stt]=255;//Data Needed
                                    	        break;
                                            case 0x2A : *stt=Dic_DD_Card_Track1;LengMin[*stt]=0 ;LengMax[*stt]=56;//DD Card (Track1)
                                    	        break;
                                            case 0x2B : *stt=Dic_DD_Card_Track2;LengMin[*stt]=0 ;LengMax[*stt]=11;//DD Card (Track2)
                                    	        break;
                                            case 0x1A : *stt=Dic_Def_UDOL;LengMin[*stt]=3 ;LengMax[*stt]=3;//Default UDOL
                                    	        break;
                                            case 0x13 : *stt=Dic_DRDOL_Rel_Data;LengMin[*stt]=0 ;LengMax[*stt]=255;//DRDOL Related Data
                                    	        break;
                                            case 0x08 : *stt=Dic_DS_AC_Type;LengMin[*stt]=1 ;LengMax[*stt]=1;//DS AC Type
                                    	        break;
                                            case 0x09 : *stt=Dic_DS_Inp_Term;LengMin[*stt]=8 ;LengMax[*stt]=8;//DS Input (Term)
                                    	        break;
                                            case 0x0A : *stt=Dic_DS_ODS_Inf_For_R;LengMin[*stt]=1 ;LengMax[*stt]=1;//DS ODS Info For Reader
                                    	        break;
                                            case 0x01 : *stt=Dic_DS_Sum_2 ;LengMin[*stt]=1 ;LengMax[*stt]=1;//DS Summary 2                   /// 8  16
                                    	        break;
                                            case 0x02 : *stt=Dic_DS_Sum_3;LengMin[*stt]=1 ;LengMax[*stt]=1;//DS Summary 3                  //
                                    	        break;
                                            case 0x0B : *stt=Dic_DS_Sum_Sta;LengMin[*stt]=1 ;LengMax[*stt]=1;//DS Summary Status
                                    	        break;
                                            case 0x0D : *stt=Dic_DSVN_Term;LengMin[*stt]=0 ;LengMax[*stt]=255;//DSVN Term
                                    	        break;
                                            case 0x15 : *stt=Dic_Err_Ind;LengMin[*stt]=6 ;LengMax[*stt]=6;//Error Indication
                                    	        break;
                                            case 0x30 : *stt=Dic_Hold_Time_Val;LengMin[*stt]=1 ;LengMax[*stt]=1;//Hold Time Value
                                    	        break;
                                            case 0x28 : *stt=Dic_IDS_Sta;LengMin[*stt]=1 ;LengMax[*stt]=1;//IDS Status
                                    	        break;
                                            case 0x1B : *stt=Dic_Ker_Con;LengMin[*stt]=1 ;LengMax[*stt]=1;//Kernel Configuration
                                    	        break;
                                            case 0x0C : *stt=Dic_Ker_ID ;LengMin[*stt]=1 ;LengMax[*stt]=1;//Kernel ID
                                    	        break;
                                            case 0x1E : *stt=Dic_Mag_Str_CVM_Cap_CVM_Req;LengMin[*stt]=1 ;LengMax[*stt]=1;//Mag-stripe CVM Capability  CVM Required
                                    	        break;
                                            case 0x2C : *stt=Dic_Mag_str_CVM_Cap_No_CVM_Req;LengMin[*stt]=1 ;LengMax[*stt]=1;//Mag-stripe CVM Capability No CVM Required
                                    	        break;
                                            case 0x33 : *stt=Dic_Max_Rel_Res_Grac_Per;LengMin[*stt]=2 ;LengMax[*stt]=2;//Maximum Relay Resistance Grace Period
                                    	        break;
                                            case 0x1C : *stt=Dic_Max_Lif_Of_Tor_Trans_Log_Rec;LengMin[*stt]=2 ;LengMax[*stt]=2;//Max Lifetime of Torn Transaction Log Record
                                    	        break;
                                            case 0x1D : *stt=Dic_Max_Num_Of_Tor_Trans_Log_Rec;LengMin[*stt]=1 ;LengMax[*stt]=1;//Max Number of Torn Transaction Log Records
                                    	        break;
                                            case 0x2D : *stt=Dic_Mes_Hold_Time;LengMin[*stt]=3 ;LengMax[*stt]=3;//Message Hold Time
                                    	        break;
                                            case 0x32 : *stt=Dic_Min_Rel_Res_Gra_Per;LengMin[*stt]=2 ;LengMax[*stt]=2;//Minimum Relay Resistance Grace Period
                                    	        break;
                                            case 0x29 : *stt=Dic_Out_Par_Set;LengMin[*stt]=8 ;LengMax[*stt]=8;//Outcome Parameter Set
                                    	        break;
                                            case 0x11 : *stt=Dic_PDOL_Rel_Data;LengMin[*stt]=0 ;LengMax[*stt]=255;//PDOL Related Data
                                    	        break;
                                            case 0x31 : *stt=Dic_Phone_Mes_Tab;LengMin[*stt]=0 ;LengMax[*stt]=255;//Phone Message Table
                                    	        break;
                                            case 0x0E : *stt=Dic_Pos_Gen_AC_Put_Data_Sta;LengMin[*stt]=1 ;LengMax[*stt]=1;//Post-Gen AC Put Data Status
                                    	        break;
                                            case 0x0F : *stt=Dic_Pre_Gen_Ac_Put_Data_Sta;LengMin[*stt]=1 ;LengMax[*stt]=1;
                                    	        break;
                                            case 0x10 : *stt=Dic_Pro_To_Fir_Wri_Flag;LengMin[*stt]=1 ;LengMax[*stt]=1;//Proceed To First Write Flag
                                    	        break;
                                            case 0x23 : *stt=Dic_Read_Con_Flo_Lim;LengMin[*stt]=6 ;LengMax[*stt]=6;//
                                    	        break;
                                            case 0x24 : *stt=Dic_Read_Con_Trans_Lim_No_CVM ;LengMin[*stt]=6 ;LengMax[*stt]=6;
                                    	        break;
                                            case 0x25 : *stt=Dic_Read_Con_Trans_Lim_CVM;LengMin[*stt]=6 ;LengMax[*stt]=6;
                                    	        break;
                                            case 0x26 : *stt=Dic_Read_CVM_Req_Lim;LengMin[*stt]=6 ;LengMax[*stt]=6;//Reader CVM Required Limit
                                    	        break;
                                            case 0x14 : *stt=Dic_Ref_Con_Par;LengMin[*stt]=1 ;LengMax[*stt]=1;//Reference Control Parameter
                                    	        break;
                                            case 0x36 : *stt=Dic_Rel_Res_Acc_Thr;LengMin[*stt]=2 ;LengMax[*stt]=2;//Reference Control Parameter
                                    	        break;
                                            case 0x37 : *stt=Dic_Rel_Res_Trans_Time_Mis_Thr;LengMin[*stt]=1 ;LengMax[*stt]=1;//Relay Resistance Transmission Time Mismatch Threshold
                                    	        break;
                                            case 0x1F : *stt=Dic_Ser_Cap;LengMin[*stt]=1 ;LengMax[*stt]=1;//Security Capability
                                    	        break;
                                            case 0x12 : *stt=Dic_Tags_To_Read;LengMin[*stt]=0 ;LengMax[*stt]=255;//Tags To Read
                                    	        break;
                                            case 0x20 : *stt=Dic_Ter_Act_Code_Def;LengMin[*stt]=5 ;LengMax[*stt]=5;//Terminal Action Code  Default
                                    	        break;
                                            case 0x21 : *stt=Dic_Ter_Act_Code_Den;LengMin[*stt]=5 ;LengMax[*stt]=5;//Terminal Action Code  Denial
                                    	        break;
                                            case 0x22 : *stt=Dic_Ter_Act_Code_Onl;LengMin[*stt]=5 ;LengMax[*stt]=5;//Terminal Action
                                    	        break;
                                            case 0x34 : *stt=Dic_CAPDU ;LengMin[*stt]=2 ;LengMax[*stt]=2;//Terminal Expected Transmission Time For Relay Resistance C-APDU
                                    	        break;
                                            case 0x35 : *stt=Dic_RAPDU;LengMin[*stt]=2 ;LengMax[*stt]=2;//Terminal Expected Transmission Time For Relay Resistance R-APDU
                                    	        break;
                                            case 0x27 : *stt=Dic_Time_Out_Val;LengMin[*stt]=2 ;LengMax[*stt]=2;//Time Out Value
                                    	        break;
                                            case 0x16 : *stt=Dic_Use_Int_Req_Data;LengMin[*stt]=22 ;LengMax[*stt]=22;//User Interface Request Data
                                    	        break;
                                            default: *stt = Dic_ERROR;
                                                break;
                                      }
                               break;
                           case 0x83:  switch(byte3){
                                            case 0x05 : *stt=Dic_Dev_Est_Trans_Time;LengMin[*stt]=2 ;LengMax[*stt]=2;RA[*stt]=1;//Device Estimated Transmission Time For Relay Resistance R-APDU
                   	                            break;
                                            case 0x02 : *stt=Dic_Dev_Rel_Res_Ent;LengMin[*stt]=4 ;LengMax[*stt]=4;RA[*stt]=1;//Device Relay Resistance Entropy
                   	                            break;
                                            case 0x04 : *stt=Dic_Max_Time_For_Pro_Rel_Res_APDU;LengMin[*stt]=2 ;LengMax[*stt]=2;RA[*stt]=1;//Max Time For Processing Relay Resistance APDU
                   	                            break;
                                            case 0x06 : *stt=Dic_Mea_Rel_Res_Pro_Time;LengMin[*stt]=2 ;LengMax[*stt]=2;//Measured Relay Resistance Processing Time
                   	                            break;
                                            case 0x03 : *stt=Dic_Min_Time_For_Pro_Rel_Res_APDU;LengMin[*stt]=2 ;LengMax[*stt]=2;RA[*stt]=1;//Min Time For Processing Relay Resistance APDU
                   	                            break;
                                            case 0x07 : *stt=Dic_RRP_Counter;LengMin[*stt]=1 ;LengMax[*stt]=1;//RRP Counter
                   	                            break;
                                            case 0x01 : *stt=Dic_Ter_Rel_Res_Ent;LengMin[*stt]=4 ;LengMax[*stt]=4;//Terminal Relay Resistance Entropy
                   	                            break;
                                            default: *stt = Dic_ERROR;
                                                break;
                                      }
                               break;
                           default: *stt = Dic_ERROR;
                               break;
                     }
              break;
         case 0xFF:  switch(byte2){
                          case 0x81:  switch(byte3){
                                           case 0x05: *stt=Dic_Data_Rec;LengMin[*stt]=0 ;LengMax[*stt]=255;//Data Record
                                               break;
                                           case 0x04: *stt=Dic_Data_To_Send;LengMin[*stt]=0 ;LengMax[*stt]=255;//Data To Send
                                               break;
                                           case 0x06: *stt=Dic_Dis_Data;LengMin[*stt]=0 ;LengMax[*stt]=255;//Discretionary Data
                                               break;
                                           case 0x03: *stt=Dic_Tags_To_Wri_Aft_Gen_AC;LengMin[*stt]=0 ;LengMax[*stt]=255;//Tags To Write After Gen AC
                                               break;
                                           case 0x02: *stt=Dic_Tags_To_Wri_Bef_Gen_AC;LengMin[*stt]=0 ;LengMax[*stt]=255;//Tags To Write Before Gen AC
                                               break;
                                           case 0x01: *stt=Dic_Torn_Rec;LengMin[*stt]=0 ;LengMax[*stt]=255;//Torn Record
                                               break;
                                           default: *stt = Dic_ERROR;
                                               break;
                                    }
                              break;
                          default: *stt = Dic_ERROR;
                              break;
                    }
              break;
         default : *stt=Dic_ERROR;
              break;
               }
}

DatabaseCheck_Boolean IsEmpty(Dic_Tag T, uint8_t Database_Array[][MAX_COL]){
  uint16_t length[MAX_COL];
  // check if the tag value is one byte, two bytes or three bytes
  // depend on the kind of tag, the length is in the second or third position in Database
  if ((Database_Array[T][0] == 0x5F)||(Database_Array[T][0] == 0x9F) || (Database_Array[T][0] == 0xBF)) {
          if (Database_Array[T][1]!= 0x81){
	      length[T] = Database_Array[T][2];
          }
          else length[T] = Database_Array[T][3];
  }
	  else if (Database_Array[T][0] == 0xDF){
	       if ((Database_Array[T][1] == 0x81)||(Database_Array[T][1] = 0x83)) {
              if (Database_Array[T][3]!= 0x81){
	          length[T] = Database_Array[T][3];
              }
              else length[T] = Database_Array[T][4];
	       }
	       else if (Database_Array[T][2] != 0x81) {
                length[T] = Database_Array[T][2];
	       }
	       else length[T] = Database_Array[T][3];
	       }
	  else if (Database_Array[T][0] == 0xFF) {
            if (Database_Array[T][3]!= 0x81){
	          length[T] = Database_Array[T][3];
            }
            else length[T] = Database_Array[T][4];
	  }
	  else if (Database_Array[T][1] != 0x81){
            length[T] = Database_Array[T][1];
	  }
	  else length[T] = Database_Array[T][2];
      if (length[T]==0){
    	 return DatabaseCheck_TRUE;
      }
  return DatabaseCheck_FALSE;
}


DatabaseCheck_Boolean IsNotPresent(Dic_Tag T, uint8_t Database_Array[][MAX_COL], uint8_t Sobyte, uint8_t *Buffertam, uint8_t Vitritag){
	  int i =0;
	  for (i =0; i<Sobyte;i++){
        if (Database_Array[T][i] != *(Buffertam + Vitritag + i))
            return DicCheck_TRUE;
	  }
	  printf("is not preent %d \n", T);
	  return DicCheck_FALSE;
}

DatabaseCheck_Boolean IsPresent(Dic_Tag T, uint8_t Database_Array[][MAX_COL], uint8_t Sobyte, uint8_t *Buffertam, uint8_t Vitritag){
	  int i =0;
	  for (i =0; i<Sobyte;i++){
        if (Database_Array[T][i] != *(Buffertam + Vitritag + i))
            return DicCheck_FALSE;
	  }
	  return DicCheck_TRUE;
}

// ****** check to get the length of tag T, number of byte and type of tag T
int Check(uint8_t *Buffertam, uint16_t *Length, uint8_t *Sobyte, CheckType *CheckTypeTag, int Vitri){
    if((*(Buffertam + Vitri) & 0x20)!= 0x00){
        *CheckTypeTag = CheckConstruct;
    }
    else *CheckTypeTag = CheckPrimiti;
    if ((*(Buffertam + Vitri) & 0x1F)!= 0x1F){
        *Sobyte = 1;
        if (*(Buffertam + Vitri +1) == 0x81){
            *Sobyte+= 1;
            *Length = *(Buffertam + Vitri +2);
        }
        else *Length = *(Buffertam + Vitri +1);
    }
    else if ((*(Buffertam + Vitri +1) & 0x80)!=0x80){
        *Sobyte =2;
        if (*(Buffertam + Vitri +2) == 0x81){
            *Sobyte += 1;
            *Length = *(Buffertam + Vitri +3);
        }
        else *Length = *(Buffertam + Vitri +2);
    }
    else {
        *Sobyte = 3;
        if (*(Buffertam + Vitri + 3) == 0x81){
            *Sobyte +=1;
            *Length = *(Buffertam + Vitri +4);
        }
        else *Length = *(Buffertam + Vitri +3);
}
}
