/*******************************************************************************
 *  (C) Copyright 2009 Molisys Solutions Co., Ltd. , All rights reserved       *
 *                                                                             *
 *  This source code and any compilation or derivative thereof is the sole     *
 *  property of Molisys Solutions Co., Ltd. and is provided pursuant to a      *
 *  Software License Agreement.  This code is the proprietary information      *
 *  of Molisys Solutions Co., Ltd and is confidential in nature.  Its use and  *
 *  dissemination by any party other than Molisys Solutions Co., Ltd is        *
 *  strictly limited by the confidential information provisions of the         *
 *  Agreement referenced above.                                                *
 ******************************************************************************/

/*
 * parse_response.c
 *
 *  Created on: 24, 8, 2017
 *      Author: Tai
 *  @brief reset database
 *         check the tag is suitable to store into database
 */
/********** Include section ***************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "parse_response.h"
#include "dictionary_parse.h"
/********** Local Constant and compile switch definition section **************/

/********** Local (static) variable definition section ************************/

/********** Local function definition section *********************************/

//************** split tag ************************

int Split_Tag(uint8_t TLV[], int ArraySize, uint8_t *Buffertam) {
    int LengSum, LengSum1 =0, LengSum2 =0, LengSum3 =0, LengSum1Temp, LengSum2Temp;
    int TagCoPos =0,TagOngPos =0, TagChaPos =0;
    int SobyteTemp1=0, SobyteTemp2 =0, LengSumTemp =0, SobyteTemp=0, i =0;
    int Sobyte =0;
    CheckType CheckType = CheckConstruct ;
    int LengthAll = ArraySize;
     /* check tag co */
    while (LengthAll){
      CheckTag(TLV, TagCoPos, &Sobyte, &LengSum, &CheckType, ArraySize);
      if((LengSum + 1 + Sobyte)> LengthAll) return 0;
      else if (CheckType == CheckConstruct){
        printf(" dia chi cua con t split %p \n", &Buffertam); // primitive1
       for (i =0; i< (Sobyte+1); i++){
        *(Buffertam) = TLV[TagCoPos + i];
          Buffertam+= 1;
       }
       printf(" dia chi tiep theo con tro %p \n", &Buffertam);
       TagCoPos += Sobyte+1;
       LengSumTemp = LengSum;
       SobyteTemp = Sobyte;
       while(LengSum>0){
    /* check tag ong */
        CheckTag(TLV, TagCoPos, &Sobyte, &LengSum1, &CheckType, ArraySize);
        if ((LengSum1 + Sobyte +1) <= LengSum){
       if (CheckType == CheckConstruct){
                    // primitive2
         for(i =0; i < (Sobyte +1); i++){
            *(Buffertam) = TLV[TagCoPos +i];
            Buffertam += 1;
         }
         LengSum1Temp = LengSum1 ; //
         SobyteTemp1 = Sobyte;
         TagOngPos = TagCoPos + Sobyte + 1;
         while(LengSum1Temp>0){
            CheckTag(TLV, TagOngPos, &Sobyte, &LengSum2, &CheckType, ArraySize);
            if ((LengSum2 + Sobyte +1)<= LengSum1Temp){

                if (CheckType == CheckConstruct){
                     LengSum2Temp=LengSum2;
                     SobyteTemp2 = Sobyte;
                     TagChaPos = TagOngPos + Sobyte +1;
                     for (i = 0; i<(Sobyte+1); i++){
                        printf(" bf %d \n", TLV[TagOngPos]);
                        *(Buffertam++) = TLV[TagOngPos +i];
                     }
                     while(LengSum2Temp){
                     CheckTag(TLV, TagChaPos, &Sobyte, &LengSum3, &CheckType, ArraySize);

                         if ((LengSum3 + Sobyte +1) <= LengSum2Temp){
                            if (CheckType == CheckConstruct) return 0;
                                StoreTagToBuffer(TagChaPos, LengSum3, TLV, Sobyte, Buffertam);
                                Buffertam += Sobyte + 1 + LengSum3;
                                LengSum2Temp -= LengSum3 + Sobyte + 1;
                                TagChaPos += Sobyte+1 + LengSum3;
                          }
                        else return 0;
                    }
                    TagOngPos += SobyteTemp2 + 1 + LengSum2;
                    LengSum1Temp -= LengSum2 + Sobyte +1;
              }
            else {
                StoreTagToBuffer(TagOngPos, LengSum2, TLV, Sobyte, Buffertam);
                Buffertam += Sobyte + 1 + LengSum2;
                TagOngPos += Sobyte +1 + LengSum2;
                LengSum1Temp -= LengSum2 + Sobyte +1;
               }
            }
            else return 0;
        }
        LengSum -= LengSum1 + SobyteTemp1 +1;
        }
       else {
          StoreTagToBuffer(TagCoPos, LengSum1, TLV, Sobyte, Buffertam);
          Buffertam += Sobyte +1 + LengSum1;
          TagCoPos += Sobyte +1 + LengSum1;
          LengSum -= LengSum1 + Sobyte +1;
        }
       }
      else return 0;
     }
      LengthAll = LengthAll -(LengSumTemp + SobyteTemp +1);
    }
    else {
      StoreTagToBuffer(TagCoPos, LengSum, TLV, Sobyte, Buffertam) ;
      Buffertam += Sobyte + LengSum;
      TagCoPos += Sobyte+ 1 + LengSum;
      LengthAll -= LengSum + Sobyte +1;
    }
    if (LengthAll ==0){
        return 1;
    }
   }
   return 0;
}
//******************** check tag to get length, type and number of byte of tag
void CheckTag(uint8_t TLV[], int Vitritag, int* Sobyte, int* Length, CheckType* CheckType, int ArraySize){
    *Sobyte =0;
    // it is construct or primitive data object
    if ((TLV[Vitritag] & 0x20)!= 0x00){
        *CheckType =  CheckConstruct;
    }
    else *CheckType = CheckPrimiti;
    // number of tag is 2 bytes of 3 bytes
    if ((TLV[Vitritag] & 0x1F) == 0x1F) {
       if ((TLV[Vitritag+1] & 0x80) == 0x80){
          *Sobyte = 3;
       }
       else *Sobyte =2;
    }
    else *Sobyte =1;
    // length of tag is bigger than 128 bytes
    if (TLV[Vitritag + *Sobyte] == 0x81){
        *Sobyte += 1;
    }
    if ((Vitritag + *Sobyte) >= ArraySize){
        *Length = LengExcess;
    }
    else *Length = TLV[Vitritag + *Sobyte];  // get length of tag
}

//****************** store tag to buffer tam *************************
void StoreTagToBuffer(int Vitritag, uint8_t LENG, uint8_t TLV[], uint8_t Sobyte, uint8_t *Buffertam){
	int s;
    int i =0;
	// save tag, length, value to pointer Buffertam
	for (i =0; i<(Sobyte +1); i++){
        *(Buffertam++) = TLV[Vitritag + i];
	}

	for(s=0; s<LENG; s++){
		*(Buffertam ++) = TLV[Vitritag+Sobyte+s+1];
	}
}
//*********** parse and store card response ****************************

ParseCheck_Boolean ParseAndStoreCardResponse(uint8_t TLV[], int Arraysize, uint8_t (*Database_Array)[MAX_COL]){

    uint16_t LengSum =0, LengSum1= 0, LengSum2 =0, LengSum3 =0;
    int TagCoPos =0, TagOngPos =0, TagChaPos =0;
    int LengSum1Temp, LengSum2Temp;
    int m =0, k =0;
    uint8_t Sobyte =0;
    CheckType Type = CheckConstruct;
    int T=0;
    int TemplateCo, TemplateOng, TemplateCha;
    // provide memory for a array pointer
    uint8_t *Buffertam = (uint8_t *)malloc(1000*sizeof(uint8_t));

    ParseCheck_Boolean TLV_EncodingError = ParseCheck_FALSE;
    // if parse TLV string is true or false
    if (Split_Tag(TLV, Arraysize, Buffertam)== 0)
       {
        TLV_EncodingError = ParseCheck_TRUE;
       }
    printf(" buffertam %d \n", Buffertam[68]);
       // it is response format 2 or not
    if ((TLV[0] != 0x6F)&& (TLV[0] != 0x70) && (TLV[0] != 0x77))
        TLV_EncodingError = ParseCheck_TRUE;
        // it is a single construct or not
    else if (TLV[1] != 0x81){
        if ((Buffertam[TLV[1]+ 2]) != 0x00)
          TLV_EncodingError = ParseCheck_TRUE;
    }
    else{
        if ((Buffertam[TLV[2]+3])!= 0x00)
            TLV_EncodingError = ParseCheck_TRUE;
    }
    if (TLV_EncodingError == ParseCheck_TRUE){
        free(Buffertam);
        return ParseCheck_FALSE;
    }
    else{
        Check(Buffertam, &LengSum, &Sobyte, &Type, 0);
        TagCoPos += Sobyte +1;
        TemplateCo = Buffertam[0];

        while(LengSum >0){
            Check(Buffertam, &LengSum1, &Sobyte, &Type, TagCoPos);
            // check type is construct or not
            if (Type == CheckConstruct){
                LengSum1Temp = LengSum1;
                k = Sobyte;
                TemplateOng = Buffertam[TagCoPos];
                TagOngPos = TagCoPos + Sobyte +1;
                while (LengSum1Temp >0 ){
                    Check(Buffertam, &LengSum2, &Sobyte, &Type, TagOngPos);
                    if (Type == CheckConstruct){
                        TemplateCha = Buffertam[TagOngPos];
                        LengSum2Temp = LengSum2;
                        m = Sobyte;
                        TagChaPos = TagOngPos + Sobyte +1;
                        while (LengSum2Temp >0){
                           Check(Buffertam, &LengSum3, &Sobyte, &Type, TagChaPos);
                           // check tag is exist in dictionary or not and check RA signal also
                           CheckIsKnownRASignal(Buffertam[TagChaPos], Buffertam[TagChaPos +1], Buffertam[TagChaPos +2], &T);
                           // condition based in TLV database
                           if (IsKnown(T)){
                             if ((IsNotPresent(T, Database_Array, Sobyte, Buffertam, TagChaPos)|| IsEmpty(T, Database_Array))&& CheckRASignal(T)&&
                                  CheckLength(T, LengSum3)&&CheckTemplate(T, TemplateCha)){
                                   StoreTagToDatabase(T, LengSum3, Database_Array, Sobyte, TagChaPos, TLV);
                                  }
                             else {free(Buffertam); return ParseCheck_FALSE;}
                           }
                           else { free(Buffertam); return ParseCheck_FALSE;}
                           LengSum2Temp -= LengSum3 + Sobyte + 1;
                           TagChaPos += Sobyte+1 + LengSum3;
                        }
                        TagOngPos += m + 1 + LengSum2;
                        LengSum1Temp -= LengSum2 + m +1;
                    }
                    // lan 2
                   else {
                      CheckIsKnownRASignal(Buffertam[TagOngPos], Buffertam[TagOngPos +1], Buffertam[TagOngPos +2], &T);
                      if (IsKnown(T)){
                         if ((IsNotPresent(T, Database_Array, Sobyte, Buffertam, TagOngPos)|| IsEmpty(T, Database_Array))&& CheckRASignal(T)&&
                            CheckLength(T, LengSum2)&&CheckTemplate(T, TemplateOng)){
                            StoreTagToDatabase(T, LengSum2, Database_Array, Sobyte, TagOngPos, TLV) ;
                         }
                         else { printf("fali r %d \n", T); free(Buffertam); return ParseCheck_FALSE;  }
                       }
                      else { free(Buffertam); return ParseCheck_FALSE;}
                      TagOngPos += Sobyte +1 + LengSum2;
                      LengSum1Temp -=   LengSum2 + Sobyte +1;
                     }
                   }
                   TagCoPos += k + 1 + LengSum1;
                   LengSum -= LengSum1 + k +1;
                }
        else {
            CheckIsKnownRASignal(Buffertam[TagCoPos], Buffertam[TagCoPos +1], Buffertam[TagCoPos +2], &T);
            if (IsKnown(T)){
              if ((IsNotPresent(T, Database_Array, Sobyte, Buffertam, TagCoPos)|| IsEmpty(T, Database_Array))&& CheckRASignal(T)&&
                 CheckLength(T, LengSum1)&&CheckTemplate(T, TemplateCo)){
                 StoreTagToDatabase(T, LengSum1, Database_Array, Sobyte, TagCoPos, TLV) ;
               }
              else { free(Buffertam); return ParseCheck_FALSE;}
            }
            else { free(Buffertam); return ParseCheck_FALSE;}
            TagCoPos += Sobyte +1 + LengSum1;
            LengSum -= LengSum1 + Sobyte +1;
        }
        }
    }
    free(Buffertam);
    return ParseCheck_TRUE;
}

//******** store tag to database ************
void StoreTagToDatabase (Dic_Tag T, uint16_t Length, uint8_t (*DatabaseArray)[MAX_COL], uint8_t Sobyte, uint8_t Vitri, uint8_t TLV[]){
    int i =0;
    for (i =0; i <(Sobyte +1 + Length); i++){
        DatabaseArray[T][i] = TLV[Vitri +i];
    }
}

void ResetDatabase(uint8_t (*Database_Array)[MAX_COL]){
    int hang, cot;
    for (hang =0; hang < MAX_ROW; hang ++){
        for (cot =0; cot < MAX_COL; cot ++){
            Database_Array[hang][cot] = 0x00;
        }
    }

}
