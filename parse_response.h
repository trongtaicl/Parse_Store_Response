
/**
 * @brief Check every tag in TLV string and after that store Length and Value of tag to database
 * - TLV string is an array that concludes Tag, Length, Value
 * - Database is an two dimensional array.
 *   Index of Row of Database array is represent for specific tag (it is defined in enumeration).
 *   Column of database array conclude tag, length, value corresponding with row.
 * - Save every tag in TLV string to an two dimensional array, this array
 *   has index of row compliant with index of row in database array.
 *   Column of this array conclude tag, length, value corresponding with row.
 * - After save TLV string into an array, check every tag is suitable for some rules and store that
 *   tag to database.
 */



/*
 * parse_response.h
 *
 *  Created on: 24 thg 8, 2017
 *      Author: Tai
 *
 */

#ifndef PARSE_RESPONSE_H_
#define PARSE_RESPONSE_H_

#ifdef __cplusplus
extern "C"
{
#endif

/********** Include section ***************************************************/
#include "dictionary_parse.h"

/********** Type definition section *******************************************/
/**
 * @brief Enumeration of result after checking tag for parsing
 */
typedef enum { ParseCheck_TRUE =1,
	           ParseCheck_FALSE =0
}ParseCheck_Boolean;

/********** Function declaration section **************************************/

/**
 * @brief split TLV string into tag, length, value
 * @param TLV[] is an array of TLV
 */

int Split_Tag(uint8_t TLV[], int ArraySize, uint8_t *Buffertam);
/**
*
*/

void StoreTagToBuffer(int Vitritag, uint8_t LENG, uint8_t TLV[], uint8_t Sobyte, uint8_t *Buffertam);
/**
*
*/

void CheckTag(uint8_t TLV[], int Vitritag, int* Sobyte, int* Length, CheckType* CheckType, int Arraysize);
/**
*
*/

void StoreTagToDatabase (Dic_Tag T, uint16_t Length, uint8_t (*DatabaseArray)[MAX_COL], uint8_t Sobyte, uint8_t Vitri, uint8_t TLV[]);
/**
*
*/

ParseCheck_Boolean ParseAndStoreCardResponse(uint8_t TLV[], int Arraysize, uint8_t (*Database_Array)[MAX_COL]);

void ResetDatabase(uint8_t (*Database_Array)[MAX_COL]);

#ifdef __cplusplus
}
#endif

#endif /* PARSE_RESPONSE_H_ */
